from django.core.files.storage import FileSystemStorage
from django.db import models

# Create your models here.
class Account(models.Model):
    fs = FileSystemStorage(location='media/images/', base_url='/media/images/')
    user = models.ForeignKey('auth.User', blank=False, on_delete=models.CASCADE, related_name='user_account')
    name = models.CharField(max_length=10, blank=True, null=True)
    nickName = models.CharField(max_length=10, blank=True, null=True)
    address = models.CharField(max_length=50, blank=True, null=True)
    thumbNail = models.ImageField(null=True, blank=True, storage=fs)
    class Meta:
        db_table = 'mgr_account'

class AuctionCategory(models.Model):
    name = models.CharField(max_length=20, null=True, blank=True)
    def __str__(self):
        return self.name
    class Meta:
        db_table = 'mgr_auction_category'

