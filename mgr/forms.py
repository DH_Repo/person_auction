from django import forms

from mgr.validators import phonenumber_validator, tel_validator


class SignUpForm(forms.ModelForm):
    uid = forms.CharField(widget=forms.TextInput, label='아이디')
    password = forms.CharField(widget=forms.PasswordInput, label='패스워드 입력')
    password1 = forms.CharField(widget=forms.PasswordInput, label='패스워드 재입력')
    email = forms.EmailField(widget=forms.EmailInput, label='이메일')


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['mobile'].validators.append(phonenumber_validator)
        self.fields['tel'].validators.append(tel_validator)


    def clean(self):
        cleaned_data = super().clean()
        id = cleaned_data.get('uid')
        if Account.objects.filter(user__username=id):
            raise forms.ValidationError("동일한 아이디가 존재합니다.")
        email = cleaned_data.get('email')
        if Account.objects.filter(user__email=email):
            raise forms.ValidationError("동일한 이메일이 존재합니다.")
        password = cleaned_data.get('password')
        password1 = cleaned_data.get('password1')
        if password is None or password1 is None:
            pass
        else:
            if password != password1:
                 raise forms.ValidationError("패스워드가 일치하지 않습니다.")

    class Meta:
        model = Account
        fields = ['name','branch','brand', 'businessLicenseNumber', 'mobile','tel','nateOnId','depositor']
        widgets = {
            'name': forms.TextInput,
            'branch': forms.Select,
            'brand': forms.TextInput,
            'businessLicenseNumber': forms.NumberInput,
            'mobile': forms.NumberInput,
            'tel': forms.NumberInput,
            'nateOnId': forms.TextInput,
            'depositor' : forms.TextInput,

        }
        labels = {
            'name': '이름',
            'branch': '지점',
            'brand': '브랜드',
            'businessLicenseNumber': '사업자번호',
            'mobile': '핸드폰',
            'tel': '매장번호',
            'nateOnId': '네이트온아이디',
            'depositor': '입금자',
        }