from django.urls import path

from service import views

app_name = 'service'
urlpatterns = [

    path('auction/registration', views.AuctionRegistrationView.as_view(), name="auction_registration"),
    path('auction/list', views.AuctionRegistrationListView.as_view(), name="auction_registration_list"),

]