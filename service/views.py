from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import FormView, TemplateView, ListView

from service.forms import AuctionRegistrationForm
from service.models import AuctionRegistration


class AuctionRegistrationView(FormView):
    template_name = 'service/auction_registration.html'
    form_class = AuctionRegistrationForm
    success_url = reverse_lazy('home')
    def get(self, request, *args, **kwargs):
        context = super().get_context_data()

        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        context = super().get_context_data()
        ar_form = AuctionRegistrationForm(request.POST)
        if ar_form.is_valid():
            return self.form_valid(ar_form)
        return self.render_to_response()

    def form_valid(self, form):
        this_form = form.save(commit=False)
        #this_form.account = self.request.user
        name = this_form.category.name
        this_form.categoryName = name
        this_form.save()
        #this_form.save_m2m()
        return super().form_valid(form)

class AuctionRegistrationListView(TemplateView):
    template_name = 'service/auction_list.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['model'] = AuctionRegistration.objects.all()
        return context




