import re
from django import forms

def phonenumber_validator(value):
    regex = re.compile('\d{10,11}$')
    if not regex.match(value):
        raise forms.ValidationError('잘못된 폰번호 입니다.')

def tel_validator(value):
    regex = re.compile('\d{7,15}$')
    if not regex.match(value):
        raise forms.ValidationError('잘못된 번호 입니다.')