# Generated by Django 3.1.7 on 2021-03-23 06:29

from django.conf import settings
import django.core.files.storage
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AuctionCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=20, null=True)),
            ],
            options={
                'db_table': 'mgr_auction_category',
            },
        ),
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=10, null=True)),
                ('nickName', models.CharField(blank=True, max_length=10, null=True)),
                ('address', models.CharField(blank=True, max_length=50, null=True)),
                ('thumbNail', models.ImageField(blank=True, null=True, storage=django.core.files.storage.FileSystemStorage(base_url='/media/images/', location='media/images/'), upload_to='')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_account', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'mgr_account',
            },
        ),
    ]
