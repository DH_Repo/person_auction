from django.core.files.storage import FileSystemStorage
from django.db import models

# Create your models here.
class AuctionRegistration(models.Model):
    lowestBid = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=0)
    dueDate = models.DateTimeField(null=True, blank=True)
    title = models.CharField(max_length=30, null=True, blank=True)
    explain = models.TextField(null=True, blank=True)
    categoryName = models.CharField(max_length=30, null=True, blank=True)
    account = models.ForeignKey('mgr.Account', blank=True,null=True, on_delete=models.SET_NULL, related_name='user_auction_registration')
    category = models.ForeignKey('mgr.AuctionCategory', blank=True,null=True, on_delete=models.SET_NULL,
                                related_name='category_auction_registration')
    class Meta:
        db_table = 'service_auction_registration'

class AuctionRegiImg(models.Model):
    fs = FileSystemStorage(location='media/images/', base_url='/media/images/')
    img = models.ImageField(null=True, blank=True, storage=fs)
    auction_regi = models.ForeignKey('AuctionRegistration', blank=False, on_delete=models.CASCADE,
                                related_name='auction_regi_img')
    class Meta:
        db_table = 'service_auction_regi_img'
