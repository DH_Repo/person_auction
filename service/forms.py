from django import forms

from service.models import AuctionRegistration


class AuctionRegistrationForm(forms.ModelForm):
    class Meta:
        model = AuctionRegistration
        exclude = ['account', 'categoryName']
        widgets = {
            "lowestBid": forms.NumberInput,
            "dueDate": forms.DateTimeInput,
            "title ": forms.TextInput,
            "explain": forms.Textarea,
            "category": forms.Select
        }
        label = {
            "lowestBid": "최저입찰가",
            "dueDate": "마감일",
            "title ": "제목",
            "explain": "내용",
            "category": "카테고리"
        }
